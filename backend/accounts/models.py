from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from tweets.models import Tweetmeme


# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.URLField(null=True, blank = True)
    points = models.IntegerField(default=0)
    most_recent_participation = models.DateField(null=True)

    def participated(self):
        self.most_recent_participation = datetime.today()

    def did_they_participate_today(self):
        if self.most_recent_participation == datetime.today():
            return True
        else:
            return False

    def __str__(self):
        return str(self.user)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
