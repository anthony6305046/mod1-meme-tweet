from django.shortcuts import render, redirect, get_object_or_404
from tweets.forms import PromptForm, TweetForm
from tweets.models import Prompt, Prefix, Suffix, Character, Tweetmeme, Vote, PromptGenerator
from random import randrange
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required(redirect_field_name="login")
def main_page(request):
    current_prompt = Prompt.objects.last()
    if Tweetmeme.objects.filter(prompt=current_prompt, user=request.user):
        return redirect("todays_tweets")
    if request.method == "POST":
        form = TweetForm(request.POST)
        if form.is_valid():
            form = form.save(False)
            form.prompt = current_prompt
            form.user = request.user
            form.save()
            return redirect("todays_tweets")

    else:
        form = TweetForm()
    context = {
        "current_prompt": current_prompt,
        "form" : form
    }
    return render(request, "tweets/new_tweet.html", context)


def gm_page(request):
    if not request.user.is_superuser:
        return redirect("main")
    current_prompt = Prompt.objects.last()
    if request.method == "POST":
        PromptGenerator.generate_prompt()
        return redirect("gm")

    context = {
        "prompt": current_prompt,
    }
    return render(request, "tweets/game_master.html", context)


@login_required(redirect_field_name="login")
def todays_tweets(request):
    current_prompt = Prompt.objects.last()
    if not Tweetmeme.objects.filter(prompt=current_prompt, user=request.user):
        return redirect("main_post")
    tweets = Tweetmeme.objects.filter(prompt=current_prompt)
    votes = Vote.objects.filter(user=request.user, prompt=current_prompt)
    tweet_votes = [vote.tweet for vote in votes]
    context = {
        "current_prompt": current_prompt,
        "tweets": tweets,
        "tweet_votes": tweet_votes,
    }
    return render(request, "tweets/todays_tweets.html", context)


@login_required(redirect_field_name="login")
def vote_on_tweet(request, id):
    voting = request.GET.get('vote')
    tweet = Tweetmeme.objects.get(id=id)
    prompt = Prompt.objects.last()
    if voting == "yes":
        vote = Vote(user=request.user, tweet=tweet, prompt=prompt)
        vote.save()
    else:
        vote = Vote.objects.get(user=request.user, tweet=tweet)
        vote.delete()
    return redirect("todays_tweets")
