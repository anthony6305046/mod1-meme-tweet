# Generated by Django 5.0 on 2024-01-04 23:08

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("tweets", "0005_tweetmeme_prompt"),
    ]

    operations = [
        migrations.AlterField(
            model_name="character",
            name="picture",
            field=models.URLField(blank=True, null=True),
        ),
    ]
