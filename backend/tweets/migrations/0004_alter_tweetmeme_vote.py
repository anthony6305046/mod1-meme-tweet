# Generated by Django 5.0 on 2024-01-04 02:06

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("tweets", "0003_character_creator"),
    ]

    operations = [
        migrations.AlterField(
            model_name="tweetmeme",
            name="vote",
            field=models.PositiveSmallIntegerField(default=0, null=True),
        ),
    ]
