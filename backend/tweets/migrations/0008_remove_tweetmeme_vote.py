# Generated by Django 5.0 on 2024-01-09 00:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tweets', '0007_vote'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tweetmeme',
            name='vote',
        ),
    ]
