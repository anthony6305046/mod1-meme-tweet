from django.db import models
from django.conf import settings
from random import randrange


class PromptGenerator(models.Model):

    @staticmethod
    def generate_prompt():
        n_num = 3
        last_n= Prompt.objects.all()[::-1][:n_num]
        random_pre = Prefix.objects.exclude(prefix__in=[prmpt.prefix.prefix for prmpt in last_n])[randrange(0,Prefix.objects.count()-n_num)]
        random_char = Character.objects.exclude(character__in=[prmpt.character.character for prmpt in last_n])[randrange(0,Character.objects.count()-n_num)]
        random_suff = Suffix.objects.exclude(suffix__in=[prmpt.suffix.suffix for prmpt in last_n])[randrange(0,Suffix.objects.count()-n_num)]
        new_prompt = f'{random_pre} {random_char} at {random_suff}'
        Prompt.objects.create(prompt=new_prompt,prefix=random_pre,character =random_char,suffix=random_suff)


class Prompt(models.Model):
    prefix = models.ForeignKey(
        "prefix",
        related_name ="prompts",
        on_delete = models.CASCADE,
        null=True,blank=True)
    character = models.ForeignKey(
        "character",
        related_name = "prompts",
        on_delete= models.CASCADE,
        null=True,blank=True
    )
    suffix =models.ForeignKey(
        "suffix",
        related_name ="prompts",
        on_delete = models.CASCADE,null=True,blank=True
    )
    prompt = models.CharField(max_length=550, null=True)
    date = models.DateField(auto_now_add=True)
    def __str__(self):
        return f"{self.prompt}"


class Prefix(models.Model):
    prefix = models.CharField(max_length =150)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name= "prefixes",
        on_delete= models.SET_NULL,
        null= True,
        blank = True,
    )
    def __str__(self):
        return self.prefix

class Suffix(models.Model):
    suffix = models.CharField(max_length =150)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name= "suffixes",
        on_delete= models.SET_NULL,
        null= True,
        blank=True,
    )
    def __str__(self):
        return self.suffix

class Character(models.Model):
    character = models.CharField(max_length=250)
    image = models.URLField(null= True, blank = True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name= "characters",
        on_delete= models.SET_NULL,
        null= True,
        blank = True,
    )
    def __str__(self):
        return self.character

class Tweetmeme(models.Model):
    tweet = models.CharField(max_length=280)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL ,
        related_name= "tweetmemes",
        on_delete= models.CASCADE,
        null= True
    )
    prompt = models.ForeignKey(
        "Prompt",
        related_name="tweetmemes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.tweet

class Vote(models.Model):
    
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="vote",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    tweet = models.ForeignKey(
        Tweetmeme,
        related_name="votes",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    prompt = models.ForeignKey(
        Prompt,
        related_name="votes",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
