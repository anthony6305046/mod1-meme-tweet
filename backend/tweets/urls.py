from django.urls import path
from tweets.views import gm_page, todays_tweets, main_page, vote_on_tweet
from django.shortcuts import redirect

def redirect_to_main_post (request):
    return redirect("main_post")

urlpatterns = [
    path("gm/",gm_page,name="gm"),
    path("todays_tweets/", todays_tweets, name="todays_tweets"),
    path("submit/",main_page,name="main_post"),
    path("",redirect_to_main_post,name="main"),
    path("<int:id>/", vote_on_tweet, name="vote_on_tweet")
]
