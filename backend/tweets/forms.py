from django.forms import ModelForm, Textarea
from tweets.models import Prompt, Tweetmeme
from django import forms

class PromptForm(ModelForm):
    class Meta:
        model =Prompt
        fields = '__all__'

class SuggestionForm(ModelForm):
    prefix = forms.CharField(max_length= 150)

class TweetForm(ModelForm):
    class Meta:
        model = Tweetmeme
        fields = [
            "tweet",
        ]
        widgets = {
            "tweet" : Textarea(attrs={'cols': 35, 'rows': 8}),
        }
