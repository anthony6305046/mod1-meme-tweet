from django.contrib import admin
from tweets.models import Prompt, Prefix, Suffix, Character, Tweetmeme, Vote
# Register your models here.

@admin.register(Prefix)
class PrefixAdmin(admin.ModelAdmin):
    list_display =[
        "prefix",
        "user",
    ]

@admin.register(Suffix)
class SuffixAdmin(admin.ModelAdmin):
    list_display =[
        "suffix",
        "user",
    ]

@admin.register(Character)
class CharacterAdmin(admin.ModelAdmin):
    list_display =[
        "character",
        "user",
    ]

@admin.register(Prompt)
class PromptAdmin(admin.ModelAdmin):
    list_display =[
        "prompt",
        "date",
    ]

@admin.register(Tweetmeme)
class Tweetmeme(admin.ModelAdmin):
    list_display =[
        "tweet",
    ]

@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = [
        "tweet",
    ]
